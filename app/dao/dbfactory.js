import MySQLConnection from "../database/MySQLConnection.js";

const connection = new MySQLConnection();

connection.connect();

connection.query('SELECT * FROM users', (err, rows) => {
    if (err) {
        console.error('Erro ao executar a consulta:', err);
        return;
    }
    console.log('Resultado da consulta:', rows);
});

connection.close();