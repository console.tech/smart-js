import MySQLConnection from '../database/MySQLConnection.js';
import bcrypt from 'bcrypt';

export default class UserDao {

    constructor(){
        this.connection = new MySQLConnection();
    }

    checkEmailExists(email, callback) {
        const sql = 'SELECT COUNT(*) AS count FROM users WHERE email = ?';
        const args = [email];
    
        // Executa a consulta para verificar se o e-mail já está cadastrado
        this.connection.query(sql, args, (err, result) => {
            if (err) {
                callback(err, null);
            } else {
                // Se o resultado retornar um valor maior que 0, significa que o e-mail já está cadastrado
                const emailExists = result[0].count > 0;
                callback(null, emailExists);
            }
        });
    }

    createUser(user, callback) {
        // Verifica se o e-mail já está cadastrado
        this.checkEmailExists(user.getEmail(), (err, emailExists) => {
            if (err) {
                // Caso ocorra um erro ao verificar o e-mail, chama o callback de erro
                callback(err, null);
            } else if (emailExists) {
                // Caso o e-mail já esteja cadastrado, retorna um erro informando que o e-mail já está em uso
                const err = new Error('E-mail já está em uso.');
                callback(err, null);
                this.connection.close();
            } else {
                // Se o e-mail não estiver cadastrado, procede com a inserção
                const sql = 'INSERT INTO users (name, email, password) VALUES (?, ?, ?)';
                
                const hashedPassword = bcrypt.hashSync(user.getPassword(), 10); // 10 é o custo do hash
                const args = [user.getName(), user.getEmail(), hashedPassword];
    
                // Conecta ao banco de dados
                this.connection.connect();
    
                // Executa a consulta
                this.connection.query(sql, args, (err, result) => {
                    // Verifica se houve algum erro na execução da consulta
                    if (err) {
                        callback(err, null);
                    } else {
                        // Retorna o ID gerado junto com o resultado da consulta
                        callback(null, { id: result.insertId, result });
                    }
    
                    // Fecha a conexão após a execução da consulta
                    this.connection.close();
                });
            }
        });
    }
    
    getUserById(){

    }

    getUserByEmail(){

    }

    updateUser(){

    }

    deleteUser(){

    }

    getAllUsers(){

    }

}
