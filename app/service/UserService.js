import User from '../domains/User.js';
import UserDao from '../dao/UserDao.js';

class UserService {

    constructor(){
        this.userDao = new UserDao();
    }

    save( user ){
        this.userDao.createUser(user, (err, result) => {
            if (err) {
                console.error(`Erro ao criar o usuário: ${err}`);
            } else {
                console.log(`Novo usuário cadastrado. ID: ${result.id}`);
            }
        });
    }
}

const user = new User( null, 'dominic', 'toreto2@local.com', 'kaito');

const service = new UserService();

service.save( user );