// 20240319_users.js

export default function createUserTable(connection) {
    connection.query(`
        CREATE TABLE IF NOT EXISTS users (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL UNIQUE,
            password VARCHAR(255) NOT NULL
        )
    `, (err, result) => {
        if (err) {
            console.error(`Erro ao criar a tabela de usuários: ${err}`);
        } else {
            console.log(`Tabela de usuários criada com sucesso!`);
        }
    });
}




