// 20240320_groups.js

export default function createUserGroupsTable(connection) {
    connection.query(`
        CREATE TABLE IF NOT EXISTS user_groups (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL UNIQUE
        )
    `, (err, result) => {
        if (err) {
            console.error(`Erro ao criar a tabela de grupos de usuários: ${err}`);
        } else {
            console.log(`Tabela de grupos de usuários criada com sucesso!`);
        }
    });
}


