// migrate.js

import fs from 'fs/promises'; // Use fs.promises para operações assíncronas de arquivo
import { fileURLToPath } from 'url';
import path from 'path';
import MySQLConnection from './MySQLConnection.js'; // Certifique-se de usar a extensão .js para módulos ESM

const connection = new MySQLConnection();

connection.connect();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const migrationsDir = path.join(__dirname, 'migrations');

fs.readdir(migrationsDir)
    .then(files => {
        files.sort(); // Garante que as migrações sejam executadas em ordem cronológica

        const executeMigrations = async () => {
            if (files.length === 0) {
                console.log('Todas as migrações foram executadas.');
                connection.close(); // Fecha a conexão quando todas as migrações forem concluídas
                return;
            }

            const migrationFile = files.shift(); // Remove o próximo arquivo de migração da lista

            const migrationPath = path.join(migrationsDir, migrationFile);
            const { default: migration } = await import(migrationPath); // Importa a migração como um módulo ECMAScript

            console.log(`Executando migração: ${migrationFile}`);

            await migration(connection); // Chama a função de migração exportada
            executeMigrations(); // Chama recursivamente para a próxima migração
        };

        executeMigrations(); // Inicia o processo de execução de migrações
    })
    .catch(err => {
        console.error('Erro ao ler o diretório de migrações:', err);
        connection.close(); // Fecha a conexão em caso de erro
    });
