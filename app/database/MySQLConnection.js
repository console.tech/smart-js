import dotenv from 'dotenv';
import mysql from 'mysql2';

dotenv.config();

export default class MySQLConnection {

    constructor(){

        const config = {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE
        };

        this.connection = mysql.createConnection( config );
        
    }

    connect(){
        this.connection.connect( (err)=> {
            if( err ){
                console.error('Erro ao conectar ao MYSQL: ', err );
                return;
            }
            console.log('Conectado ao MYSQL!');
        })
    }

    query( sql, args, callback){
        this.connection.query( sql, args, (err, rows)=> {
            if( err ){
                console.error('Erro ao executar a consulta:', err);
                return;
            }
            callback( null, rows);
        } )
    }

    close(){
        this.connection.end( (err)=> {
            if( err ){
                console.error('Erro ao fechar conexão!');
                return;
            }
            console.log('Conexão com o MySQL encerrada!');
        })
    }
}

