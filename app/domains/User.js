export default class User {

    constructor( id, name, email, password ){
        
        let _id = id;
        let _name = name;
        let _email = email;
        let _password = password;

        this.getId = ()=> {
            return _id;
        }

        this.setId = ( id )=> {
            _id = id;
        }

        this.getName = ()=> {
            return _name;
        }

        this.setName = ( name )=> {
            _name = name;
        }

        this.getEmail = ()=> {
            return _email;
        }

        this.setEmail = ( email )=> {
            _email = email;
        }

        this.getPassword = ()=> {
            return _password;
        }

        this.setPassword = ( password )=> {
            _password = password;
        }
    }

    static createUserWithIdAndName(id, name) {
        return new User(id, name, null);
    }

    static createUserWithEmail(email) {
        return new User(null, null, email);
    }

    print() {
        return ` ${this.getId()}, ${this.getName()}, ${this.getEmail()} `;
    }

}

